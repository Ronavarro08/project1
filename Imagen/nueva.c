#include <tiffio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define uint32 unsigned long

uint32 width;
uint32 height;

int main(void)
{
    TIFF* tif = TIFFOpen("exa.tif", "r");
    TIFF* out = TIFFOpen("new.tif","w");


	TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &width);           // uint32 width;
        TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &height);        // uint32 height;

	TIFFSetField (out, TIFFTAG_IMAGEWIDTH, width);  // set the width of the image
        TIFFSetField(out, TIFFTAG_IMAGELENGTH, height);    // set the height of the image
        TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, 4);   // set number of channels per pixel
        TIFFSetField(out, TIFFTAG_BITSPERSAMPLE, 8);    // set the size of the channels
        TIFFSetField(out, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);    // set the origin of the image.
        //   Some other essential fields to set that you do not have to understand for now.
        TIFFSetField(out, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
        TIFFSetField(out, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
        TIFFSetField(out, TIFFTAG_COMPRESSION, 1);




    if (tif) {
        uint32 imagelength;
        tsize_t scanline;
        unsigned char *buf = NULL;
        uint32 row;
        uint32 col;

        TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imagelength);
        scanline = TIFFScanlineSize(tif);
	printf("ROw: %ld, Col: %ld", imagelength, col);
        buf = (unsigned char *) _TIFFmalloc(scanline);
        for (row = 0; row < imagelength; row++)
        {
	    printf("\nEn: %ld\n", row);
            TIFFReadScanline(tif, buf, row, 0);
	    //printf("\n\nEl tama;o es: %d", sizeof(buf));
            TIFFWriteScanline(out, buf, row, 0);
	    //_TIFFfree(buf);

	    for (col = 0; col < scanline; col++)
              printf("%c", buf[col]);


            printf("\n");
        }
        _TIFFfree(buf);
        TIFFClose(tif);
	TIFFClose(out);
    }
    return 0;
}
