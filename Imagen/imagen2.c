#include <stdio.h>
#include <tiffio.h>
#include <stdlib.h>

 
int main()
{
 
double red_mean;                // mean red value of fed image
 
    uint32 r;                       // row index
    uint32 c;                       // column index
    uint32 rows;                    // number of rows in image
    uint32 columns;                 // number of columns in image
    uint16 BitsPerSample;           // normally 8 for grayscale image
    uint16 SamplesPerPixel;         // normally 1 for grayscale image
    uint16 PhotoMetric;             // normally 1 for grayscale image
    unsigned char *in_image;        // pointer for input image array
    unsigned char *out_image;       // pointer for output image array
    TIFF *in_filep;                 // handle for input image file
    TIFF *out_filep;                // handle for output image file
    int check_io;                   // status of I/O operation
 
    // Open input image file
    in_filep = TIFFOpen("exa.tif", "r");
    if (in_filep == NULL)
    printf("Could not open input file!");
 
    // Open output image file
    out_filep = TIFFOpen ("output.tif", "w"); 
    if (out_filep == NULL)
    printf("Could not open output file!");
 
    // Determine the size of the input image
    TIFFGetField(in_filep, TIFFTAG_IMAGELENGTH, &rows);
    TIFFGetField(in_filep, TIFFTAG_IMAGEWIDTH, &columns);
    TIFFGetField(in_filep, TIFFTAG_BITSPERSAMPLE, &BitsPerSample);
    TIFFGetField(in_filep, TIFFTAG_SAMPLESPERPIXEL, &SamplesPerPixel);
    TIFFGetField(in_filep, TIFFTAG_PHOTOMETRIC, &PhotoMetric);
    //TIFFGetField(in_filep, TIFFTAG_PHOTOMETRIC, &PlanarConfig);
    // The following statements are helpful in debugging
    printf("rows = %ld\n", rows);
    printf("columns = %ld\n", columns);
    printf("BitsPerSample = %d\n", BitsPerSample);
    printf("SamplesPerPixel = %d\n", SamplesPerPixel);
    printf("PhotoMetric = %d\n", PhotoMetric);
 
    // Specify TIFF header fields for output image
    TIFFSetField(out_filep, TIFFTAG_IMAGELENGTH, rows);
    TIFFSetField(out_filep, TIFFTAG_IMAGEWIDTH, columns);
    TIFFSetField(out_filep, TIFFTAG_BITSPERSAMPLE, BitsPerSample);
    TIFFSetField(out_filep, TIFFTAG_SAMPLESPERPIXEL, SamplesPerPixel);
    TIFFSetField(out_filep, TIFFTAG_PLANARCONFIG, 1);
    TIFFSetField(out_filep, TIFFTAG_PHOTOMETRIC, PhotoMetric);
 
 
    // Allocate memory to hold image arrays
    in_image = (unsigned char *) _TIFFmalloc(rows*columns);
    if (in_image == NULL) 
    printf("Could not allocate memory!");
    out_image = (unsigned char *) _TIFFmalloc(rows*columns);
    if (out_image == NULL) 
    printf("Could not allocate memory!");
 
    // Read image pixel values from file, row by row 
    for (r = 0; r < rows; r++)
    {
        check_io = TIFFReadScanline(in_filep, &in_image[r*columns], r, 1);
        if (check_io != 1)
        printf("Could not read image from file!");
    }
     
//************************************************************************************
    // Output the first several samples (pixel elements) to screen
    //red_mean = 0;
    //for (r = 0; r < 3; r++)
       // for (c = 0; c < 24; c++)
        //for (c = 0; c <= (columns*3); (c=c+3))
        //{
          //  cout << "r = " << r << " c= " << c << " Intensity = " << (int) in_image[r*columns+c] << endl;
        //}       
//************************************************************************************
    
    // Write new image to file, row by row 
    for (r = 0; r < rows; r++)
    {
        check_io = TIFFWriteScanline(out_filep, &out_image[r*columns], r, 1);
        if (check_io != 1)
        printf("Could not read image from file!");
    }
 
    // Deallocate the image memory, close file streams, and exit
    _TIFFfree(in_image);
    _TIFFfree(out_image);
    TIFFClose(in_filep);
    TIFFClose(out_filep);
    exit (0);
}
 
/*****************************************************
Function name:   print_error
Description:
    Print an error message, and exit the program.
Input parameters:
    err_message -- pointer to string to be printed
Returned value:
    none
***************************************************/
