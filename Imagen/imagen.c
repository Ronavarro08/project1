#include <tiffio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define uint32 unsigned long
uint32 width;
uint32 height;
uint32 pixelito;
uint32 pix;
uint32 comp;

int main(void){
	TIFF*tif= TIFFOpen("exa.tif", "r");
	
	// #define uint32 unsigned long
	TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &width);           // uint32 width;
	TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &height);        // uint32 height;

	TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &pixelito);
	TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &pix);

	TIFFGetField(tif, TIFFTAG_COMPRESSION, &comp);

	printf("Samples per pixel: %ld", pixelito);
	printf("\n\nBits per sample: %ld", pix);

	printf("\n\nCompression: %ld", comp);

	printf("\n\n%ld",width);
	//printf(height);
	//width = width/2;
	//height = height/2;

	uint32 npixels=width*height;
	uint32 *raster=(uint32 *) _TIFFmalloc(npixels *sizeof(uint32));

	int cool = TIFFReadRGBAImage(tif, width, height, raster, 0);

	//TIFFRGBAImageGet(tif, raster, width , height);

	printf("\n\ncool: %d", cool);
	
	
	int i;
	for(int total = 0; total < npixels; total++){		
		
		char R=(char )TIFFGetR(raster[total]);  // where X can be the channels R, G, B, and A.

		char G=(char )TIFFGetG(raster[total]);  // where X can be the channels R, G, B, and A.

		char B=(char )TIFFGetB(raster[total]);  // where X can be the channels R, G, B, and A.

		char A=(char )TIFFGetA(raster[total]);  // where X can be the channels R, G, B, and A.

		printf("\nEn el pixel: %d, estan los sig valores: R:%d G:%d B:%d A:%d", total, R, G, B, A);

	}

	
	//_TIFFfree(raster);

	TIFFClose(tif);

	// Aqui empieza a guardar...
	
	
	TIFF *out= TIFFOpen("new.tif", "w");

	int sampleperpixel = 4;    // or 3 if there is no alpha channel, you should get a understanding of alpha in class soon. 

	char new[width*height*sampleperpixel];

	printf("Aqui sigue funcionando");

	int n = 0;

	for(int total = 0; total < npixels; total++){

		new[n+0]=(char )TIFFGetR(raster[total]);  

		new[n+1]=(char )TIFFGetG(raster[total]);  

		new[n+2]=(char )TIFFGetB(raster[total]);

		new[n+3]=(char )TIFFGetA(raster[total]);  

		n+=4;

	}


	char *image= new; 

	TIFFSetField (out, TIFFTAG_IMAGEWIDTH, width);  // set the width of the image
	TIFFSetField(out, TIFFTAG_IMAGELENGTH, height);    // set the height of the image
	TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, sampleperpixel);   // set number of channels per pixel
	TIFFSetField(out, TIFFTAG_BITSPERSAMPLE, 8);    // set the size of the channels
	TIFFSetField(out, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);    // set the origin of the image.
	//   Some other essential fields to set that you do not have to understand for now.
	TIFFSetField(out, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
	TIFFSetField(out, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
	TIFFSetField(out, TIFFTAG_COMPRESSION, 1);

	tsize_t linebytes = sampleperpixel * width;     // length in memory of one row of pixel in the image.

	unsigned char *buf = NULL;        // buffer used to store the row of pixel information for writing to file
	//    Allocating memory to store the pixels of current row
	if (TIFFScanlineSize(out)*linebytes)
	    buf =(unsigned char *)_TIFFmalloc(linebytes);
	else
	    buf = (unsigned char *)_TIFFmalloc(TIFFScanlineSize(out));

	// We set the strip size of the file to be size of one row of pixels
	TIFFSetField(out, TIFFTAG_ROWSPERSTRIP, TIFFDefaultStripSize(out, width*sampleperpixel));

	//Now writing image to the file one strip at a time
	
	for (uint32 row = 0; row < height; row++)
	{
		memcpy(buf, &image[(height-row-1)*linebytes], linebytes);    // check the index here, and figure out why not using h*linebytes
		    
		if (TIFFWriteScanline(out, buf, row, 0) < 0)
		break;
	}
	(void) TIFFClose(out);

	if (buf)
	    _TIFFfree(buf);

	return 0;
}
//gcc -o imagen imagen.c -ltiff
//https://research.cs.wisc.edu/graphics/Courses/638-f1999/libtiff_tutorial.htm
//sudo apt-get update -y
//sudo apt-get install -y libtiff-dev
//

